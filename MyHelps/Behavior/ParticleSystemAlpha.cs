﻿using UnityEngine;
using System.Collections;

public class ParticleSystemAlpha : MonoBehaviour
{
    static public void SetParticleSystemAlpha(GameObject obj, float alpha)
    {
        if (obj == null)
            return;
        ParticleSystem[] ps = obj.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem p in ps)
        {
            ParticleSystem.MainModule main = p.main;
            Color c = main.startColor.color;
            main.startColor = new Color(c.r, c.g, c.b, alpha);
        }
    }
}