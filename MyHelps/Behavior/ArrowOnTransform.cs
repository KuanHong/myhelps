﻿using UnityEngine;
using System.Collections;

namespace MyHelps
{
    public class ArrowOnTransform : MonoBehaviour
    {
        private Transform _follow;
        public AudioSource HitSound;

        // Use this for initialization
        void Start()
        {
            if (HitSound != null)
                HitSound.PlayOneShot(HitSound.clip, 0.3f);
            
        }

        public void SetFollow(Transform follow)
        {
            _follow = follow;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (_follow == null)
            {
                Destroy(gameObject);
                return;
            }
            transform.position = _follow.position;
            transform.rotation = _follow.rotation;
        }
    }

}