﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// PrefabManager是每個scene主要放prefab連結的地方，讓prefab可以被build出來
/// </summary>

public class PrefabManager : MySingleton<PrefabManager>
{
    /// <summary>
    /// 包含預先生成的Prefab資訊
    /// </summary>
    [System.Serializable]
    public class POOL_DATA
    {
        //public bool usePool;

        [Tooltip("初始POOL的數量(0為不作pool)")]
        public int poolSize;
        public GameObject prefab;
    }

    public enum EnemyTag
    {
        Monster,
        ThrowWeapon,
        MagicWeapon,
        BullKingAttack
    };

    /// <summary>
    /// 拿來控制產生殘影與打擊怪物的門檻值
    /// </summary>
    public const float WeaponThreshold = 0.8f;  //不揮棒0.01 ~ 1.2揮棒//7f;//要是使用rigidbody的速率要使用7

    /// <summary>
    /// 法宝悟空的飞行时间
    /// </summary>
    public const float WuKongFlyTime = 2f;

    //要新增prefab，請寫一下說明，範例prefab是拿來做啥用的，之後所有要使用的prefab都像這樣宣告並且將新的prefab，
    //拉進PrefabManager的prefab裡面
    public GameObject MonkeyKingBarPrefab;
    public GameObject MonkeyKingCloudPrefab;

    //[Tooltip("云彩")]
    //public GameObject CloudPrefab;

    [Tooltip("Road Section Head")]
    public GameObject RoadSectionHead;

    [Tooltip("Road Section Tail")]
    public GameObject RoadSectionTail;

    [Tooltip("Road Section Loop")]
    public GameObject[] RoadSectionLoop;

    [Tooltip("红孩儿")]
    public POOL_DATA RedBoy;

    [Tooltip("牛魔王")]
    public POOL_DATA BullKing;

    [Tooltip("铁扇公主")]
    public POOL_DATA MPrincess;

    [Tooltip("場景的景(頭)")]
    public GameObject SceneHead;

    [Tooltip("場景的景(尾)")]
    public GameObject SceneTail;

    [Tooltip("場景的近景(中間亂數部分)")]
    public GameObject[] SceneLoop;

    [Tooltip("头显附加UI效果")]
    public GameObject CameraUI;

    [Tooltip("Combo字体效果")]
    public GameObject[] ComboShow;

    [Tooltip("打击计分统计")]
    public GameObject ScoreShow;

    [Tooltip("用来存进场的playgroundparticle")]
    public GameObject PlayGroundParticle;

    [Tooltip("用来提示打擊節奏")]
    public GameObject NoticeHitParticle;

    [Tooltip("游戏开始或结束菜单UI")]
    public GameObject GameStartOrOverUI;

    [Tooltip("金箍棒特效prefab")]
    public GameObject[] MonkeykingBarRFX;

    [Tooltip("金箍棒揮動音效")]
    public AudioClip[] MonkeykingBarSwingAudio;
    [Tooltip("金箍棒打击音效")]
    public AudioClip[] MonkeykingBarAudio;
    [Tooltip("玩家受击音效")]
    public AudioClip[] PlayerBehitAudio;
    [Tooltip("怪物动画帧事件所发出的音效")]
    public AudioClip[] MonsterAnimatorAudio;
    [Tooltip("游戏开始音效")]
    public AudioClip GameStartAudio;
    [Tooltip("游戏结束音效")]
    public AudioClip GameEndAudio;

    [Tooltip("金箍棒打击折射效果")]
    public GameObject HitDistortObject;

    [Tooltip("音乐管理器")]
    public GameObject Mp3Player;

    [Tooltip("做camera blending的shader")]
    public Shader PostRenderFlareShader;

    [Tooltip("怪物")]

    public POOL_DATA ClonedMonkeyPrefab;
    public POOL_DATA WuKongPrefab;

    //public POOL_DATA Spider_L;
    public POOL_DATA Spider;
    //public POOL_DATA YuMianHu_L;
    public POOL_DATA YuMianHu;
    public POOL_DATA Cattle_Flip;
    //public POOL_DATA Cattle_L;
    public POOL_DATA Cattle;
    //public POOL_DATA Cattle_Jump;
    //public POOL_DATA Bear_L;
    public POOL_DATA BBear;
    public POOL_DATA BBear_Rock;
    public POOL_DATA DaPengJing;
    //public POOL_DATA GoldKing;
    //public POOL_DATA SilverKing;
    public POOL_DATA Peri;

    public POOL_DATA EnemyEnterMeteoricPool;
    public GameObject EnemyEnterMeteoric;

    Dictionary<string, GameObject> _prefabRec = new Dictionary<string, GameObject>();
    Dictionary<string, POOL_DATA> _prefabPool = new Dictionary<string, POOL_DATA>();

    /// <summary>
    /// 根據變數宣告名稱，取得prefab game object
    /// </summary>
    GameObject _tryGetPrefab(string name)
    {
        GameObject prefabObj = null;
        if (!_prefabRec.TryGetValue(name, out prefabObj))
        {
            prefabObj = MyReflection.GetMemberVariable<GameObject>(this, name) as GameObject;
            //不管是否為null,都將prefab的結果作紀錄，避免再次使用reflection
            //if (prefabObj == null)
            //    Debug.Log("[PrefabManager][_tryGetPrefab] prefabObj : null");
            //else
            //    Debug.Log("[PrefabManager][_tryGetPrefab] prefabObj : " + prefabObj);
            _prefabRec.Add(name, prefabObj);
        }
        return prefabObj;
    }

    /// <summary>
    /// 根據變數宣告名稱，取得prefab game object
    /// </summary>
    GameObject _tryGetPoolPrefab(string name)
    {
        GameObject prefabObj = null;
        POOL_DATA poolData;
        if (!_prefabPool.TryGetValue(name, out poolData))
        {
            poolData = MyReflection.GetMemberVariable<POOL_DATA>(this, name) as POOL_DATA;
            if (poolData == null)
                Debug.LogWarning("[PrefabManager] [_tryGetPoolPrefab] poolData null : " + name + " , from class : " + this.GetType());

            //不管是否為null,都將prefab的結果作紀錄，避免再次使用reflection
            _prefabPool.Add(name, poolData);
        }

        if (poolData != null)
            prefabObj = poolData.prefab;
        return prefabObj;
    }

    void Awake()
    {
        //單獨建立一個PoolManager物件給prefab manager使用
        PoolManager.CreateSingleton("[PrefabManager]->Pool");
        PoolManager.Instance.ShowDebug = true;

        //預先對pool作處理
        //掃過所有POOL_DATA成員，判斷之中誰要做Pool，然後預先生成POOL
        string[] memberNames;
        POOL_DATA[] datas = MyReflection.GetMemberVariable<POOL_DATA>(this, out memberNames);
        int count = 0;

        foreach (POOL_DATA data in datas)
        {
            Debug.Log("[PrefabManager] pooling : " + memberNames[count]);
            if (data.prefab != null)
            {
                Debug.Log("[PrefabManager] pooling variable : " + memberNames[count] + " , prefab : " + data.prefab.name + " , size : " + data.poolSize);
                if (data.poolSize > 0)
                {
                    //使用prefab建立pool
                    PoolManager.Instance.GetObjectPool(data.prefab, data.poolSize);
                    Debug.LogWarning("[PrefabManager] create pool done: " + data.prefab.name);
                }
                else
                {
                    Debug.LogError("[PrefabManager] pool size is 0 : " + data.prefab.name);
                }
            }
            count++;
        }
    }

    /// <summary>
    /// 使用refection去針對變數名稱取得prefab gameobject
    /// </summary>
    public GameObject Spawn(string name, Vector3 pos, Quaternion rot)
    {
        //使用refection取得 POOL_DATA，然後判斷是否有做POOL，使用instantiate或是從pool manager取得
        Debug.Log("[PrefabManager] Spawn Prefab : " + name);

        GameObject spawnObj = null;
        GameObject prefab = _tryGetPrefab(name);
        if (prefab != null)
        {
            //Debug.Log("[PrefabManager] _tryGetPrefab : " + prefab.name);
            spawnObj = Instantiate(prefab, pos, rot);
            Debug.Log("[PrefabManager] spawn from Instantiate hash : " + spawnObj.GetHashCode());
        }
        else
        {
            prefab = _tryGetPoolPrefab(name);
            if (prefab == null)
            {
                Debug.LogError("[PrefabManager] _tryGetPoolPrefab fail, variable: " + name);
                return null;
            }
            spawnObj = PoolManager.Instance.Spawn(prefab, pos, rot);
            Debug.Log("[PrefabManager] spawn from PoolManager hash : " + spawnObj.GetHashCode() + " , name : " + spawnObj.name);
        }

        //關閉所有renderer影子及接受影子
        //if (spawnObj.transform.GetComponentInChildren<SkinnedMeshRenderer>() != null)//怪物角色把他影子打開
        //    RagDollSetting._setRendererCastShadow(spawnObj.transform, true);
        //else
            RagDollSetting._setRendererCastShadow(spawnObj.transform, false);
        RagDollSetting._setRendererRecieveShadow(spawnObj.transform, false);
        RagDollSetting._setRendererMotionVectors(spawnObj.transform, false);
        RagDollSetting._setLightShadow(spawnObj.transform, false);
        RagDollSetting._setRendererLightProb(spawnObj.transform, false);
        RagDollSetting._setRendererReflectionProb(spawnObj.transform, false);
        return spawnObj;
    }

    /// <summary>
    /// 判斷物件生成方式決定使用destroy或是deactivate
    /// </summary>
    public void DeSpawn(GameObject obj, float sec = 0)
    {
        Debug.Log("[PrefabManager] DeSpawn from PoolManager hash : " + obj.GetHashCode() + " , name : " + obj.name);
        if (PoolManager.Instance.IsInPool(obj))
            PoolManager.Instance.DeSpawn(obj, sec);
        else
            Destroy(obj, sec);
    }
}
