﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RagDollSetting : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    [ContextMenu("Close all use gravity")]
    void CloseAllUseGravity()
    {
        _setAllUseGravity(transform, false);
    }

    [ContextMenu("Open all use gravity")]
    void OpenAllUseGravity()
    {
        _setAllUseGravity(transform, true);
    }

    static void _setAllUseGravity(Transform parent, bool value)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
            rb.useGravity = value;

        for (int a = 0; a < parent.childCount; a++)
            _setAllUseGravity(parent.GetChild(a), value);
    }

    [ContextMenu("Set all isKinematic")]
    void SetIsKinematic()
    {
        _setIsKinematic(transform, true);
    }

    [ContextMenu("Set all notKinematic")]
    void SetNotKinematic()
    {
        _setIsKinematic(transform, false);
    }

    public static void _setIsKinematic(Transform parent, bool value)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
            rb.isKinematic = value;

        for (int a = 0; a < parent.childCount; a++)
            _setIsKinematic(parent.GetChild(a), value);
    }

    [ContextMenu("Set all ContinuousDynamic")]
    void SetContinuousDynamic()
    {
        _setContinuousDynamic(transform, true);
    }

    [ContextMenu("Set all No ContinuousDynamic")]
    void SetNoContinuousDynamic()
    {
        _setContinuousDynamic(transform, false);
    }

    public static void _setContinuousDynamic(Transform parent, bool value)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
        {
            if (value)
            {
                Debug.LogWarning("_setContinuousDynamic true" + parent.name);
                rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            }
            else
            {
                Debug.LogWarning("_setContinuousDynamic false" + parent.name);
                rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
            }
        }

        for (int a = 0; a < parent.childCount; a++)
            _setContinuousDynamic(parent.GetChild(a), value);
    }

    [ContextMenu("Set all Continuous collision")]
    void SetContinuousCollision()
    {
        _setContinuousCollision(transform);
    }

    public static void _setContinuousCollision(Transform parent)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
        {
            Debug.LogWarning("_setContinuousCollision " + parent.name);
            rb.collisionDetectionMode = CollisionDetectionMode.Continuous;

        }
        for (int a = 0; a < parent.childCount; a++)
            _setContinuousCollision(parent.GetChild(a));
    }

    [ContextMenu("Set all Interpolate")]
    void SetInterpolate()
    {
        _setInterpolate(transform, true);
    }

    [ContextMenu("Set all Interpolate None")]
    void SetInterpolateNone()
    {
        _setInterpolate(transform, false);
    }

    static void _setInterpolate(Transform parent, bool value)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
        {
            if (value)
                rb.interpolation = RigidbodyInterpolation.Interpolate;
            else
                rb.interpolation = RigidbodyInterpolation.None;
        }

        for (int a = 0; a < parent.childCount; a++)
            _setInterpolate(parent.GetChild(a), value);
    }

    [ContextMenu("Set all FreezeAll")]
    void SetFreezeAll()
    {
        _setAllConstraintFreezeAll(transform, true);
    }

    [ContextMenu("Set all No FreezeAll")]
    void SetNoFreezeAll()
    {
        _setAllConstraintFreezeAll(transform, false);
    }

    public static void _setAllConstraintFreezeAll(Transform parent, bool freeze)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
        {
            if (freeze)
                rb.constraints = RigidbodyConstraints.FreezeAll;
            else
                rb.constraints = RigidbodyConstraints.None;
        }

        for (int a = 0; a < parent.childCount; a++)
            _setAllConstraintFreezeAll(parent.GetChild(a), freeze);
    }

    //[ContextMenu("AntiRagdollStretch.cs Add all ")]
    //void AddAntiRagdollStretch()
    //{

    //    _setAntiRagdollStretch(transform, true);
    //}

    //[ContextMenu("AntiRagdollStretch.cs Remove all")]
    //void RemoveAntiRagdollStretch()
    //{
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //    _setAntiRagdollStretch(transform, false);
    //}

    //static void _setAntiRagdollStretch(Transform parent, bool value)
    //{
    //    Rigidbody rb = parent.GetComponent<Rigidbody>();
    //    if (rb != null)
    //    {
    //        if (value)
    //        {
    //            Debug.LogWarning("_setAntiRagdollStretch true : " + parent.name);
    //            parent.gameObject.AddComponent<AntiRagdollStretch>();
    //        }
    //        else
    //        {
    //            Debug.LogWarning("_setAntiRagdollStretch false" + parent.name);
    //            AntiRagdollStretch anti = parent.gameObject.GetComponent<AntiRagdollStretch>();
    //            if (anti != null)
    //                DestroyImmediate(anti);
    //        }
    //    }

    //    for (int a = 0; a < parent.childCount; a++)
    //        _setAntiRagdollStretch(parent.GetChild(a), value);
    //}


    [ContextMenu("Add all Joint Enable projection")]
    void AddJointEnableProjection()
    {

        _setJointEnableProjection(transform, true);
    }

    [ContextMenu("remove all Joint Enable projection")]
    void RemoveJointEnableProjection()
    {
        _setJointEnableProjection(transform, false);
    }

    static void _setJointEnableProjection(Transform parent, bool value)
    {
        CharacterJoint cj = parent.GetComponent<CharacterJoint>();
        if (cj != null)
        {
            if (value)
            {
                Debug.LogWarning("_setJointEnableProjection true : " + parent.name);
                cj.enableProjection = true;
            }
            else
            {
                Debug.LogWarning("_setJointEnableProjection false : " + parent.name);
                cj.enableProjection = false;
            }
        }

        for (int a = 0; a < parent.childCount; a++)
            _setJointEnableProjection(parent.GetChild(a), value);
    }

    [ContextMenu("Set Renderer Cast Shadow")]
    void SetRendererCastShadow()
    {
        _setRendererCastShadow(transform, true);
    }

    [ContextMenu("Set Renderer Cast Shadow 2 side")]
    void SetRendererCastShadow2Side()
    {
        _setRendererCastShadow(transform, true, true);
    }

    [ContextMenu("Set Renderer Not Cast Shadow")]
    void SetRendererNotCastShadow()
    {
        _setRendererCastShadow(transform, false);
    }

    public static void _setRendererCastShadow(Transform parent, bool value, bool twoSide = false)
    {
        Renderer cj = parent.GetComponent<Renderer>();
        if (cj != null)
        {
            if (value)
            {
                if (twoSide)
                {
                    //Debug.LogWarning("_setRendererCastShadow true : " + parent.name + "2 Side");
                    cj.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
                }
                else
                {
                    //Debug.LogWarning("_setRendererCastShadow true : " + parent.name);
                    cj.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                }
            }
            else
            {
                //Debug.LogWarning("_setRendererCastShadow false : " + parent.name);
                cj.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
        }

        for (int a = 0; a < parent.childCount; a++)
            _setRendererCastShadow(parent.GetChild(a), value, twoSide);
    }

    [ContextMenu("Set Renderer Recieve Shadow")]
    void SetRendererRecieveShadow()
    {
        _setRendererRecieveShadow(transform, true);
    }

    [ContextMenu("Set Renderer Not Recieve Shadow")]
    void SetRendererNotRecieveShadow()
    {
        _setRendererRecieveShadow(transform, false);
    }

    public static void _setRendererRecieveShadow(Transform parent, bool value)
    {
        Renderer cj = parent.GetComponent<Renderer>();
        if (cj != null)
        {
            if (value)
            {
                //Debug.LogWarning("_setRendererRecieveShadow true : " + parent.name);
                cj.receiveShadows = true;
            }
            else
            {
                //Debug.LogWarning("_setRendererRecieveShadow false : " + parent.name);
                cj.receiveShadows = false;
            }
        }

        for (int a = 0; a < parent.childCount; a++)
            _setRendererRecieveShadow(parent.GetChild(a), value);
    }

    public static void _setLightShadow(Transform parent, bool value)
    {
        Light light = parent.GetComponent<Light>();
        if (light != null)
        {
            light.shadows = (value) ? LightShadows.Soft : LightShadows.None;
        }

        for (int a = 0; a < parent.childCount; a++)
            _setLightShadow(parent.GetChild(a), value);
    }

    [ContextMenu("Set Renderer Not Render Motion Vectors")]
    void SetRendererNotMotionVectors()
    {
        _setRendererMotionVectors(transform, false);
    }

    [ContextMenu("Set Renderer Render Motion Vectors")]
    void SetRendererMotionVectors()
    {
        _setRendererMotionVectors(transform, true);
    }

    public static void _setRendererMotionVectors(Transform parent, bool value)
    {
        Renderer cj = parent.GetComponent<Renderer>();
        if (cj != null)
        {
            cj.motionVectorGenerationMode = (value) ? MotionVectorGenerationMode.Camera : MotionVectorGenerationMode.ForceNoMotion;
        }

        for (int a = 0; a < parent.childCount; a++)
            _setRendererMotionVectors(parent.GetChild(a), value);
    }

    [ContextMenu("Set Renderer Not Light Prob")]
    void SetRendererNotLightProb()
    {
        _setRendererLightProb(transform, false);
    }

    public static void _setRendererLightProb(Transform parent, bool value)
    {
        Renderer cj = parent.GetComponent<Renderer>();
        if (cj != null)
        {
            cj.lightProbeUsage = (value) ? UnityEngine.Rendering.LightProbeUsage.BlendProbes : UnityEngine.Rendering.LightProbeUsage.Off;
        }

        for (int a = 0; a < parent.childCount; a++)
            _setRendererLightProb(parent.GetChild(a), value);
    }

    [ContextMenu("Set Renderer Not Reflection Prob")]
    void SetRendererNotReflectionProb()
    {
        _setRendererReflectionProb(transform, false);
    }

    public static void _setRendererReflectionProb(Transform parent, bool value)
    {
        Renderer cj = parent.GetComponent<Renderer>();
        if (cj != null)
        {
            cj.reflectionProbeUsage = (value) ? UnityEngine.Rendering.ReflectionProbeUsage.BlendProbesAndSkybox : UnityEngine.Rendering.ReflectionProbeUsage.Off;
        }

        for (int a = 0; a < parent.childCount; a++)
            _setRendererReflectionProb(parent.GetChild(a), value);
    }

    [ContextMenu("Set All Static")]
    void SetAllStatic()
    {
        _setAllStatic(transform, true);
    }

    [ContextMenu("Set All Not Static")]
    void SetAllNotStatic()
    {
        _setAllStatic(transform, false);
    }

    static void _setAllStatic(Transform parent, bool value)
    {
        parent.gameObject.isStatic = value;

        for (int a = 0; a < parent.childCount; a++)
            _setAllStatic(parent.GetChild(a), value);
    }
#if UNITY_EDITOR
    [ContextMenu("Set Ready to Bake Shadow Map")]
    void SetBakeShadowMap()
    {
        List<Light> lights = MyHelpNode.FindTypesList<Light>(transform);
        Debug.LogWarning("[SetBakeShadowMap] bakes scene contains " + lights.Count + " lights");
        foreach (Light l in lights)
        {
            l.gameObject.SetActive(true);
            l.lightmapBakeType = LightmapBakeType.Baked;
        }

        SetAllStatic();
        SetRendererCastShadow2Side();
        SetRendererRecieveShadow();
    }

    [ContextMenu("Set Shadow Map Baked OK to close cast shadow")]
    void SetBakeShadowMapDone()
    {
        List<Light> lights = MyHelpNode.FindTypesList<Light>(transform);
        Debug.LogWarning("[SetBakeShadowMap] bake scene contains " + lights.Count + " lights");
        foreach (Light l in lights)
        {
            l.gameObject.SetActive(false);
            l.lightmapBakeType = LightmapBakeType.Baked;
        }
        SetRendererNotCastShadow();
    }
#endif
    /// <summary>
    /// 設定Ragdoll的屬性
    /// </summary>
    [ContextMenu("Set Ragdoll")]
    void SetRagdollSetting()
    {
        OpenAllUseGravity();
        SetNotKinematic();
        //SetIsKinematic();
        SetNoContinuousDynamic();
        SetInterpolateNone();
        //SetNoFreezeAll();
        SetFreezeAll();
        AddJointEnableProjection();

        Animator animator = GetComponent<Animator>();
        animator.updateMode = AnimatorUpdateMode.Normal;
        animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
    }

    /// <summary>
    /// 設定Ragdoll的屬性
    /// </summary>
    [ContextMenu("Set Mesh Collider")]
    void SetMeshCollider()
    {
        MyHelpMesh._setMeshCollider(transform, false);
    }

    /// <summary>
    /// 設定Ragdoll的屬性
    /// </summary>
    [ContextMenu("Set Mesh Collider Destroy")]
    void ClearMeshCollider()
    {
        MyHelpMesh._setMeshCollider(transform, true);
    }


    /// <summary>
    /// 將src整棵樹的Rigidbody，設定給dest整棵樹
    /// </summary>
    public static void CopyTreeRigidbody(Transform srcRoot, Transform destRoot)
    {
        //不能用GetComponents，要用GetComponentsInChildren才可以发现没有active的物体的组件
        Rigidbody[] srcRB = srcRoot.GetComponentsInChildren<Rigidbody>();
        Rigidbody[] destRB = destRoot.GetComponentsInChildren<Rigidbody>();
        if (srcRB.Length != destRB.Length)
        {
            Debug.LogError("[CopyTreeRigidbody] srcRB.Length : " + srcRB.Length + " != destRB.Length : " + destRB.Length);
            MyMessageBox.Show("src:" + srcRoot.name + " , dest:" + destRoot.name,
                "[CopyTreeRigidbody] srcRB.Length : " + srcRB.Length + " != destRB.Length : " + destRB.Length);
        }

        for (int a = 0; a < srcRB.Length; a++)
        {
            destRB[a].useGravity = srcRB[a].useGravity;
            destRB[a].isKinematic = srcRB[a].isKinematic;
            destRB[a].interpolation = srcRB[a].interpolation;
            destRB[a].collisionDetectionMode = srcRB[a].collisionDetectionMode;
            destRB[a].constraints = srcRB[a].constraints;
        }
    }

    public static void _setAllRigidBodySleep(Transform parent, bool SleepOrWakeup)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
            if (SleepOrWakeup) rb.Sleep(); else rb.WakeUp();

        for (int a = 0; a < parent.childCount; a++)
            _setAllRigidBodySleep(parent.GetChild(a), SleepOrWakeup);
    }

    /// <summary>
    /// 關閉所有的光與影
    /// </summary>
    [ContextMenu("Close All Light Shadow")]
    void CloseAllEffectLightShadow()
    {
        SetRendererNotCastShadow();
        SetRendererNotRecieveShadow();
        SetRendererNotLightProb();
        SetRendererNotReflectionProb();
        SetRendererNotMotionVectors();
    }
}
