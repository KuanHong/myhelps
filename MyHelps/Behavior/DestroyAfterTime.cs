﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {
  public float ExistTime = 0.0f;
  // Use this for initialization
	void Start () {
    Destroy(gameObject, ExistTime);
	}

}
