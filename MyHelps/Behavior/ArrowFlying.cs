﻿using UnityEngine;
using System.Collections;
using MyHelps;

namespace MyHelps
{
    public class ArrowFlying : MonoBehaviour
    {
        Vector3 _newPos, _oldPos, _velocity;

        public float Speed;
        public float ArrowGravity;
        public LayerMask ArrowLayerMask;

        //public float HitForceApply;
        public float FlyLifeSec = 1;
        float _lifeCount;

        public delegate void OnArrowHit(RaycastHit hit, Ray hitRay);
        OnArrowHit _onArrowHit;

        public void SetArrowHitEvent(OnArrowHit arrowHit) { SetArrowHitEvent(arrowHit, 1.0f); }
        public void SetArrowHitEvent(OnArrowHit arrowHit, float SpeedRatio)
        {
            _onArrowHit = arrowHit;
            Speed *= Mathf.Pow(SpeedRatio, 2);
        }

        // Use this for initialization
        void Start()
        {
            // Debug.Log("ArrowFlying Start");
            //Debug.Break();
            _newPos = transform.position;
            _oldPos = _newPos;
            _velocity = Speed * transform.forward;
        }

        // Update is called once per frame
        void Update()
        {
            _lifeCount += Time.deltaTime;
            if (_lifeCount > FlyLifeSec)
            {
                Destroy(this.gameObject);
                return;
            }

            _newPos += _velocity * Time.deltaTime;
            Vector3 dir = _newPos - _oldPos;
            float dist = dir.magnitude;
            dir /= dist;

            if (dist > 0)
            {
                RaycastHit hit;
                Ray hitRay = new Ray(_oldPos, dir);
                if (Physics.Raycast(hitRay, out hit, dist, ArrowLayerMask))
                {
                    GameObject hitPoint = new GameObject("hitpoint");
                    hitPoint.transform.position = hit.point;
                    hitPoint.transform.rotation = transform.rotation;
                    hitPoint.transform.parent = hit.transform;

                    ArrowOnTransform arrowOnTransform = this.GetComponent<ArrowOnTransform>();
                    arrowOnTransform.SetFollow(hitPoint.transform);
                    arrowOnTransform.enabled = true;

                    if (_onArrowHit != null)
                        _onArrowHit(hit, hitRay);

                    //if (hit.rigidbody != null)
                    //    hit.rigidbody.AddForceAtPosition(HitForceApply * dir, hit.point);

                    enabled = false;
                    _newPos = hit.point;
                }
            }

            _oldPos = transform.position;
            transform.position = _newPos;
            _velocity.y -= ArrowGravity * Time.deltaTime;
            transform.rotation = Quaternion.LookRotation(dir);
        }
    }

}