﻿#define UseSoundManagerxx
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAudio : MonoBehaviour {

    public void playaudio(string audioname)
    {
#if UseSoundManager
        SoundManager.Instance.PlaySound(audioname);
        return;
#endif
        //目前只有牛魔王一隻在用EVENT撥放音效，之後再改善
        AudioSource As = GetComponentInChildren<AudioSource>();
        As.PlayOneShot(SoundManager.Instance.TryGetAudioClip(audioname), 1.0f);
    }
}
