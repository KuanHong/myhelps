using System.IO;
using UnityEngine;

public class CubemapSaver : MonoBehaviour
{
    private Cubemap mCubemap;

    void OnDestroy()
    {
        if (mCubemap != null)
        {
            Debug.Log("OnDestroy() release cubemap");
            DestroyImmediate(mCubemap);
            mCubemap = null;
        }
    }
    
    public void UpdateCubemap(ref Cubemap outCubemap)
    {
        if (outCubemap == null)
            outCubemap = new Cubemap(2048, TextureFormat.RGB24, false);

        Camera camera = this.gameObject.GetComponent<Camera>();
        camera.backgroundColor = Color.black;
        camera.cullingMask = ~(1 << 8);
        camera.RenderToCubemap(outCubemap);  
    }

    void LateUpdate()
    {
        if (Input.GetKeyUp("space"))
        {
            UpdateCubemap(ref mCubemap);
            SaveCubemap();
        }
    }
    
    void SaveCubemap()
    {
        Debug.Log("save cube map start");
        Texture2D tex = new Texture2D(mCubemap.width, mCubemap.height, TextureFormat.RGB24, false);

        _saveCubeMapFace2File(tex, CubemapFace.PositiveX);
        _saveCubeMapFace2File(tex, CubemapFace.PositiveY);
        _saveCubeMapFace2File(tex, CubemapFace.PositiveZ);
        _saveCubeMapFace2File(tex, CubemapFace.NegativeX);
        _saveCubeMapFace2File(tex, CubemapFace.NegativeY);
        _saveCubeMapFace2File(tex, CubemapFace.NegativeZ);

        DestroyImmediate(tex);
        Debug.Log("save cube map done!!");
    }

    void _saveCubeMapFace2File(Texture2D tex, CubemapFace face)
    {
        // Read screen contents into the texture        
        tex.SetPixels(mCubemap.GetPixels(face));
        // Encode texture into PNG
        var bytes = tex.EncodeToPNG();
		File.WriteAllBytes("d:/" + mCubemap.name + face.ToString() + ".png", bytes);
    }

	

}