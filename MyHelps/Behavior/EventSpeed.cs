﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSpeed : MonoBehaviour
{
    public delegate void OnSpeedRecoverCallback();
    public OnSpeedRecoverCallback OnSpeedRecover;

    //float recSpeed;
    void speed(string speed_sec)
    {
        string[] sss = speed_sec.Split('_');
        float speed = float.Parse(sss[0]);
        timeUpSec = float.Parse(sss[1]);

        //if (DQGameClient.Instance.IsGameStage2)
        //    timeUpSec /= BullkingAI.BossStage2SpeedUp;//與撥動畫的速度同步scale

        //Animator ani = GetComponent<Animator>();
        //recSpeed = ani.speed;
        //DOTween.To(() => ani.speed, x => ani.speed = x, speed, 0.5f); //0.5秒到內插植
        //ani.speed = speed;
        BullkingAI.SpeedBuff buff = new BullkingAI.SpeedBuff();
        buff.id = BullkingAI.SpeedBuffID.eventSpeed;
        buff.ratio = speed;
        if (BullkingAI.SpeedBuffList.ContainsKey(buff.id))
            Debug.LogError("[EventSpeed] SpeedBuffList 裡面已經存在EventSpeed的改變速度 : " + BullkingAI.SpeedBuffList[buff.id].ratio);
        BullkingAI.SpeedBuffList.Add(buff.id, buff);

        //Debug.LogWarning("[EventSpeed][speed] : " + speed + ", [recSpeed] : " + recSpeed + ", [ani.speed] : " + ani.speed);
        //StartCoroutine(_recoverSpeed(sec));

        timeCount = 0;
        timeUp = false;
    }

    bool timeUp;
    float timeCount;
    float timeUpSec; //基於baseSpeed和eventSpeed的情況要持續的時間
    void Update()
    {
        if (timeUp)
            return;

        float changeSpeed = 1;
        Dictionary<BullkingAI.SpeedBuffID, BullkingAI.SpeedBuff>.Enumerator itr = BullkingAI.SpeedBuffList.GetEnumerator();
        while (itr.MoveNext())
        {
            if (itr.Current.Key != BullkingAI.SpeedBuffID.baseSpeed && itr.Current.Key != BullkingAI.SpeedBuffID.eventSpeed)
                changeSpeed *= itr.Current.Value.ratio;
        }

        timeCount += Time.deltaTime * changeSpeed;
        if (timeCount > timeUpSec)
        {
            _recoverSpeed();
            timeUp = true;
        }
    }

    IEnumerator _recoverSpeed(float sec)
    {
        yield return new WaitForSeconds(sec);
        //Animator ani = GetComponent<Animator>();
        //DOTween.To(() => ani.speed, x => ani.speed = x, recSpeed, 0.5f); //0.5秒到內插植
        //ani.speed = recSpeed;
        _recoverSpeed();
    }

    void _recoverSpeed()
    {
        BullkingAI.SpeedBuffList.Remove(BullkingAI.SpeedBuffID.eventSpeed);
        //Debug.LogWarning("[EventSpeed][_recoverSpeed] [ani.speed] : " + ani.speed + ", [recSpeed] : " + recSpeed);
        if (OnSpeedRecover != null)
            OnSpeedRecover();
    }
}
