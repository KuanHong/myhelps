﻿using UnityEngine;
using System.Collections;

public static class MyHelpDraw
{
    public static void LineDraw(Transform start, Vector3 end, float startW, float endW, Color color)
    {
        LineRenderer lineRender = MyHelpNode.FindOrAddComponent<LineRenderer>(start);
        lineRender.enabled = true;
        lineRender.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        lineRender.receiveShadows = false;

        //add line renderer        
        lineRender.SetWidth(startW, endW);
        lineRender.SetPosition(0, start.transform.position);
        lineRender.SetPosition(1, end);
        //lineRender.SetColors(color, color);
        if (lineRender.material.shader.name != "Unlit/Texture")
        {
            lineRender.material = new Material(Shader.Find("Unlit/Texture"));
            Texture2D redTex = new Texture2D(2, 2, TextureFormat.RGB565, false);
            Color fillColor = new Color(1.0f, 0.0f, 0.0f);
            Color[] fillColorArray = redTex.GetPixels();
            for (int i = 0; i < fillColorArray.Length; ++i)
                fillColorArray[i] = fillColor;
            redTex.SetPixels(fillColorArray);
            redTex.Apply();
            lineRender.material.SetTexture("_MainTex", redTex);
            Debug.Log("[MyHelpDraw][LineDraw] add material 'Unlit/Texture'");
        }
        //MyHelpDraw.SetColor_Standard(lineRender.material, color);
        
    }

    public static void LineDraw2nd(Transform start, Vector3 end)
    {
        LineRenderer lineRender = start.gameObject.GetComponent<LineRenderer>();
        lineRender.SetVertexCount(4);
        lineRender.SetPosition(2, start.transform.position);
        lineRender.SetPosition(3, end);
    }

    public static void LineDrawClose(Transform start)
    {
        if (start == null) return;

        LineRenderer lineRender = start.gameObject.GetComponent<LineRenderer>();
        if (lineRender != null)
            lineRender.enabled = false;
    }

    public static Shader GetShader_DisableZ()
    {
        Shader shader = Shader.Find("Hidden/DisableZ");
        if (shader == null)
            Debug.LogError("[GetShader_DisableZ] fail --> Shader.Find(Hidden/DisableZ)");
        return shader;
    }

    public static void SetColor_DisableZ(Material mat, Color color)
    {
        if (mat == null)
            Debug.LogError("[SetColor_DisableZ] fial --> mat == null");

        mat.SetColor("_Color", new Vector4(color.r, color.g, color.b, color.a));
    }

    public static void SetColor_Standard(Material mat, Color color)
    {
        if (mat.shader.name != "Standard")
            Debug.LogWarning("[SetColor_Standard] : mat.shader.name != 'Standard'");
        mat.SetColor("_Color", new Vector4(color.r, color.g, color.b, color.a));
    }
}
