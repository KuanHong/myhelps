﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class MyReflection
{
    /// <summary>
    /// http://stackoverflow.com/questions/7649324/c-sharp-reflection-get-field-values-from-a-simple-class
    /// 根據變數名稱取得成員Obj指標
    /// </summary>
    public static object GetMemberVariable<T>(object src, string propName)
    {
        object final = null;
        Type t = src.GetType();
        //PropertyInfo pi = t.GetProperty(propName);
        //if (pi == null)
        //    Debug.LogError("[MyReflection] [GetProperty] : null : " + t.ToString() + " , propName : " + propName);
        //return pi.GetValue(src, null);

        FieldInfo fields = t.GetField(propName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        if (fields != null)
        {
            final = fields.GetValue(src);
            //Debug.Log("[MyReflection][GetMemberVariable] variable : " + fields.Name);
        }

        if (final == null)
        {
            //Debug.LogError("[MyReflection][GetMemberVariable]GetField.GetValue fail : " + typeof(T));
        }
        else if (final is T)
        {
            //Debug.LogWarning("[MyReflection][GetMemberVariable]cast success, type : " + final.GetType() + ", T :" + typeof(T));
            return final;
        }
        else
        {
            //Debug.LogError("[MyReflection][GetMemberVariable]cast fail, type : " + final.GetType() + ", T :" + typeof(T));
        }
        return null;
    }

    /// <summary>
    /// https://msdn.microsoft.com/en-us/library/system.reflection.fieldinfo.getvalue(v=vs.110).aspx
    /// 由傳入的class instance(src)找出我要的類型成員(T)
    /// </summary>
    public static T[] GetMemberVariable<T>(object src, out string[] memberNames)
    {
        List<string> memberNameList = new List<string>();

        List<T> outList = new List<T>();
        FieldInfo[] classFields = src.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        foreach (FieldInfo classField in classFields)
        {
            object field = classField.GetValue(src);
            if (field is T)
            {
                outList.Add((T)field);
                memberNameList.Add(classField.Name);
            }
        }
        memberNames = memberNameList.ToArray();
        return outList.ToArray();
    }
}
