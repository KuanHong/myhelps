﻿#define USE_UGUI
#if UNITY_EDITOR || USE_UGUI
using Unitycoding.UIWidgets;
#else
using System.Windows.Forms;
#endif

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MyMessageBox
{
    public enum BoxIcon
    {
        Information,
        Warning,
    }

    public static void Show(string titile, string content, BoxIcon icon = BoxIcon.Information)
    {
#if UNITY_EDITOR || USE_UGUI
        GameObject messageBoxCanvas = GameObject.Find("MessageBoxCanvas");
        if(messageBoxCanvas == null)
            messageBoxCanvas = GameObject.Find("MessageBoxCanvas(Clone)");
        if (messageBoxCanvas == null)
            messageBoxCanvas = GameObject.Instantiate<GameObject>(Resources.Load("MessageBoxCanvas") as GameObject);

        Transform DimmerTransform = null;
        MyHelpNode.FindTransform(messageBoxCanvas.transform, "Dimmer", ref DimmerTransform);

        if (DimmerTransform.gameObject.activeSelf)//如果已經在顯示messagebox，就不要在秀新的了
            return;

        //Transform IconTransform = null;
        //MyHelpNode.FindTransform(messageBoxCanvas.transform, "Icon", ref IconTransform);
        //Debug.Log(IconTransform.name);
        //MessageBoxExample boxEx = IconTransform.GetComponent<MessageBoxExample>();
        //Debug.Log(boxEx.name);
        Sprite iconSprite = null;// boxEx.icon;//直接拿information的圖來用(不管warning了)

        //Transform megBoxObj = null;
        //MyHelpNode.FindTransform(messageBoxCanvas.transform, "MessageBox", ref megBoxObj);
        //GameObject newObj= GameObject.Instantiate(megBoxObj.gameObject);
        //newObj.transform.parent = megBoxObj.parent;
        //newObj.name += Random.Range(0, 10000);

        //MessageBox messageBox = UIUtility.Find<MessageBox>(newObj.name);
        MessageBox messageBox = UIUtility.Find<MessageBox>("MessageBox");
        messageBox.Show(titile, content, iconSprite, null, new string[] { "OK" });
#else
        //TopMost http://blog.csdn.net/rockstones/article/details/12176641
        MessageBox.Show(content, titile, MessageBoxButtons.OK,
            (icon == BoxIcon.Information) ? MessageBoxIcon.Information : MessageBoxIcon.Warning,
            MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
        //MessageBox.Show(titile, content);
#endif

        Debug.LogWarning("MessageBox:[" + titile + "] : " + content);
    }
}
