﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MyHelps
{
    public static class MyHelp
    {
        public static string GetLastString(string text, char lastSimbol)
        {
            int i = text.LastIndexOf(lastSimbol);
            if (i < 0)
                return text;
            return text.Substring(i, text.Length - i);
        }

        public static GameObject ResourceLoadInChild(Transform parent, string pathRes)
        {
            GameObject obj = MonoBehaviour.Instantiate(Resources.Load(pathRes)) as GameObject;
            obj.transform.parent = parent.transform;
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
            return obj;
        }

        public static Vector3 Clamp(Vector3 value, Vector3 vecMin, Vector3 vecMax)
        {
            value.x = Mathf.Clamp(value.x, vecMin.x, vecMax.x);
            value.y = Mathf.Clamp(value.y, vecMin.y, vecMax.y);
            value.z = Mathf.Clamp(value.z, vecMin.z, vecMax.z);
            return value;
        }

        public static bool IsLegalFloat(float f)
        {
            return !(
                float.IsInfinity(f) ||
                float.IsNaN(f) ||
                float.IsNegativeInfinity(f) ||
                float.IsPositiveInfinity(f)
                );
        }

        public static bool IsLegalVector3(Vector3 v)
        {
            return (
                IsLegalFloat(v.x) &&
                IsLegalFloat(v.y) &&
                IsLegalFloat(v.z)
                );
        }

        ////http://forum.unity3d.com/threads/how-to-assign-matrix4x4-to-transform.121966/
        //public static Quaternion QuaternionFromMatrix(ref Matrix4x4 matrix)
        //{
        //    var qw = Mathf.Sqrt(1f + matrix.m00 + matrix.m11 + matrix.m22) / 2;
        //    var w = 4 * qw;
        //    var qx = (matrix.m21 - matrix.m12) / w;
        //    var qy = (matrix.m02 - matrix.m20) / w;
        //    var qz = (matrix.m10 - matrix.m01) / w;

        //    return new Quaternion(qx, qy, qz, qw);
        //}

        public static Quaternion QuaternionFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 forward;
            forward.x = matrix.m02;
            forward.y = matrix.m12;
            forward.z = matrix.m22;

            Vector3 upwards;
            upwards.x = matrix.m01;
            upwards.y = matrix.m11;
            upwards.z = matrix.m21;

            return Quaternion.LookRotation(forward, upwards);
        }



        /// <summary>
        /// http://answers.unity3d.com/questions/836915/smallest-bounding-sphere.html
        /// </summary>
        public struct BoundingSphere
        {
            public Vector3 center;
            public float radius;
            public BoundingSphere(Vector3 aCenter, float aRadius)
            {
                center = aCenter;
                radius = aRadius;
            }

            public static BoundingSphere Calculate(IEnumerable<Vector3> aPoints)
            {
                Vector3 xmin, xmax, ymin, ymax, zmin, zmax;
                xmin = ymin = zmin = Vector3.one * float.PositiveInfinity;
                xmax = ymax = zmax = Vector3.one * float.NegativeInfinity;
                foreach (var p in aPoints)
                {
                    if (p.x < xmin.x) xmin = p;
                    if (p.x > xmax.x) xmax = p;
                    if (p.y < ymin.y) ymin = p;
                    if (p.y > ymax.y) ymax = p;
                    if (p.z < zmin.z) zmin = p;
                    if (p.z > zmax.z) zmax = p;
                }
                var xSpan = (xmax - xmin).sqrMagnitude;
                var ySpan = (ymax - ymin).sqrMagnitude;
                var zSpan = (zmax - zmin).sqrMagnitude;
                var dia1 = xmin;
                var dia2 = xmax;
                var maxSpan = xSpan;
                if (ySpan > maxSpan)
                {
                    maxSpan = ySpan;
                    dia1 = ymin; dia2 = ymax;
                }
                if (zSpan > maxSpan)
                {
                    dia1 = zmin; dia2 = zmax;
                }
                var center = (dia1 + dia2) * 0.5f;
                var sqRad = (dia2 - center).sqrMagnitude;
                var radius = Mathf.Sqrt(sqRad);
                foreach (var p in aPoints)
                {
                    float d = (p - center).sqrMagnitude;
                    if (d > sqRad)
                    {
                        var r = Mathf.Sqrt(d);
                        radius = (radius + r) * 0.5f;
                        sqRad = radius * radius;
                        var offset = r - radius;
                        center = (radius * center + offset * p) / r;
                    }
                }
                return new BoundingSphere(center, radius);
            }
        }

        public static Plane TransformPlane(Matrix4x4 M, Plane plane)
        {
            Vector4 P = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);
            return TransformPlane(M, P);
        }

        public static Plane TransformPlane(Matrix4x4 M, Vector4 P)
        {
            //http://stackoverflow.com/questions/7685495/transforming-a-3d-plane-by-4x4-matrix

            float dis = P.w;
            Vector3 dir = (Vector3)P;
            dir.Normalize();

            Vector3 v = dir * dis;
            Vector4 O = new Vector4(v.x, v.y, v.z, 1);
            Vector4 N = new Vector4(dir.x, dir.y, dir.z, 0);
            O = M * O;
            N = Matrix4x4.Transpose(Matrix4x4.Inverse(M)) * N;

            //O = transform.TransformPoint(v);
            //N = transform.TransformDirection(dir);     

            //xyz = N.xyz
            float d = Vector3.Dot(O, N);
            Plane plane = new Plane(N, d);

            //You now have a new normal vector N and a new position vector O
            //MyHelp.DrawPlane(O, plane.normal, Color.green);

            return plane;
        }

        public static void DrawPlane(Plane plane, Vector3 basePosition)
        {
            Plane collidePlane = plane;
            Vector3 collideDir = plane.normal * -1.0f;
            collidePlane.normal = plane.normal;// * -1.0f;

            float collideDis;
            if (collidePlane.Raycast(new Ray(basePosition, collideDir), out collideDis))
            {
                Vector3 colPos = basePosition + collideDir * collideDis;
                MyHelp.DrawPlane(colPos, plane.normal, Color.yellow);
                // Debug.DrawLine(planePosition, colPos, Color.white);
            }
            else
            {
                collidePlane.normal = plane.normal;// * -1.0f;
                collideDir = plane.normal;
                if (collidePlane.Raycast(new Ray(basePosition, collideDir), out collideDis))
                {
                    Vector3 colPos = basePosition + collideDir * collideDis;
                    MyHelp.DrawPlane(colPos, plane.normal, Color.magenta);
                }
                else
                    Debug.Log("MyHelp::DrawPlane::plane.Raycast fail... collideDis : " + collideDis);
            }
        }

        /// <summary>
        /// http://answers.unity3d.com/questions/467458/how-to-debug-drawing-plane.html
        /// </summary>
        public static void DrawPlane(Vector3 position, Vector3 normal, Color color)
        {
            Vector3 v3;
            // if (normal.normalized != Vector3.forward)
            //     v3 = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
            // else
            //      v3 = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude;

            v3 = Vector3.Cross(normal, Vector3.up);
            v3 = Vector3.Cross(normal, v3).normalized * normal.magnitude;

            var corner0 = position + v3;
            var corner2 = position - v3;
            var q = Quaternion.AngleAxis(90.0f, normal);
            v3 = q * v3;
            var corner1 = position + v3;
            var corner3 = position - v3;

            Debug.DrawLine((corner0), (corner2), color);
            Debug.DrawLine((corner1), (corner3), color);
            Debug.DrawLine((corner0), (corner1), color);
            Debug.DrawLine((corner1), (corner2), color);
            Debug.DrawLine((corner2), (corner3), color);
            Debug.DrawLine((corner3), (corner0), color);
            Debug.DrawRay((position), (normal), Color.red);
        }

        /// <summary>
        /// http://answers.unity3d.com/questions/461588/drawing-a-bounding-box-similar-to-box-collider.html
        /// </summary>
        public class ShowMeshBounds
        {
            public Color color = Color.green;

            private Vector3 v3FrontTopLeft;
            private Vector3 v3FrontTopRight;
            private Vector3 v3FrontBottomLeft;
            private Vector3 v3FrontBottomRight;
            private Vector3 v3BackTopLeft;
            private Vector3 v3BackTopRight;
            private Vector3 v3BackBottomLeft;
            private Vector3 v3BackBottomRight;

            //void Update()
            //{

            //    DrawBox();
            //}

            void CalcPositons(Bounds bounds, Matrix4x4 parentWorldMatrix)
            {
                // Bounds bounds = GetComponent<MeshFilter>().mesh.bounds;

                //Bounds bounds;
                //BoxCollider bc = GetComponent<BoxCollider>();
                //if (bc != null)
                //    bounds = bc.bounds;
                //else
                //return;

                Vector3 v3Center = bounds.center;
                Vector3 v3Extents = bounds.extents;

                v3FrontTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top left corner
                v3FrontTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top right corner
                v3FrontBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom left corner
                v3FrontBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom right corner
                v3BackTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top left corner
                v3BackTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top right corner
                v3BackBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom left corner
                v3BackBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom right corner

                v3FrontTopLeft = parentWorldMatrix.MultiplyPoint(v3FrontTopLeft); //transform.TransformPoint(v3FrontTopLeft);
                v3FrontTopRight = parentWorldMatrix.MultiplyPoint(v3FrontTopRight);
                v3FrontBottomLeft = parentWorldMatrix.MultiplyPoint(v3FrontBottomLeft);
                v3FrontBottomRight = parentWorldMatrix.MultiplyPoint(v3FrontBottomRight);
                v3BackTopLeft = parentWorldMatrix.MultiplyPoint(v3BackTopLeft);
                v3BackTopRight = parentWorldMatrix.MultiplyPoint(v3BackTopRight);
                v3BackBottomLeft = parentWorldMatrix.MultiplyPoint(v3BackBottomLeft);
                v3BackBottomRight = parentWorldMatrix.MultiplyPoint(v3BackBottomRight);
            }

            public void DrawBox(Bounds bounds, Matrix4x4 parentWorldMatrix)
            {
                CalcPositons(bounds, parentWorldMatrix);

                //if (Input.GetKey (KeyCode.S)) {
                Debug.DrawLine(v3FrontTopLeft, v3FrontTopRight, color);
                Debug.DrawLine(v3FrontTopRight, v3FrontBottomRight, color);
                Debug.DrawLine(v3FrontBottomRight, v3FrontBottomLeft, color);
                Debug.DrawLine(v3FrontBottomLeft, v3FrontTopLeft, color);

                Debug.DrawLine(v3BackTopLeft, v3BackTopRight, color);
                Debug.DrawLine(v3BackTopRight, v3BackBottomRight, color);
                Debug.DrawLine(v3BackBottomRight, v3BackBottomLeft, color);
                Debug.DrawLine(v3BackBottomLeft, v3BackTopLeft, color);

                Debug.DrawLine(v3FrontTopLeft, v3BackTopLeft, color);
                Debug.DrawLine(v3FrontTopRight, v3BackTopRight, color);
                Debug.DrawLine(v3FrontBottomRight, v3BackBottomRight, color);
                Debug.DrawLine(v3FrontBottomLeft, v3BackBottomLeft, color);
                //}
            }

        }


    }

}