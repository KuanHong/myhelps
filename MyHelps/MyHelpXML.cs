﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System;
using System.Text;

namespace MyHelps
{
    /// <summary>
    /// https://dotblogs.com.tw/yc421206/2011/05/20/25595
    /// </summary>
    public static class MyHelpXML
    {
        public static readonly string WorkingDirectory = Directory.GetCurrentDirectory() + "/";

        /// <summary>
        /// 取得目前工作目錄下的完整路徑
        /// </summary>
        public static string GetFullPath(string filename)
        {
            return WorkingDirectory + filename;
        }

        public static bool SaveXMLFile(string fileName, object xmlData)
        {
            FileStream FP = File.Open(fileName, FileMode.Create);
            if (FP != null)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(xmlData.GetType());
                xmlSerializer.Serialize(FP, xmlData);
                FP.Close();
                FP.Dispose();
                xmlSerializer = null;
                Debug.Log("[SaveXMLFile] success : " + fileName);
                return true;
            }
            Debug.LogError("[SaveXMLFile] fail : " + fileName);
            return false;
        }

        public static T LoadXMLFile<T>(string fileName)
        {
            if (File.Exists(fileName))
            {
                FileStream fp = null;
                StreamReader reader = null;
                XmlSerializer xmlSerializer = null;
                try
                {
                    fp = File.Open(fileName, FileMode.Open);
                    xmlSerializer = new XmlSerializer(typeof(T));
                    reader = new StreamReader(fp, Encoding.UTF8);
                    object obj = (T)xmlSerializer.Deserialize(reader);

                    if (obj == null)
                        return default(T);
                    else
                    {
                        Debug.Log("[LoadXMLFile] success : " + fileName);
                        return (T)obj;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("[MyHelpXML] : " + e.Message);
                }
                finally
                {
                    fp.Close();
                    fp.Dispose();
                    reader.Close();
                    reader.Dispose();
                    xmlSerializer = null;
                }
            }
            else
            {
                Debug.LogError("[MyHelpXML] : " + fileName + " File not found!!!");
            }
            return default(T);
        }
    }
}