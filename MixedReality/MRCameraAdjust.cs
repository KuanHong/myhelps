﻿using UnityEngine;
using System.Collections;

public class MRCameraAdjust : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SteamVR_Render.instance.externalCamera.transform.childCount == 0)
        {
            return;
        }
        Transform MROffset = SteamVR_Render.instance.externalCamera.transform.GetChild(0);
        Camera MRCamera = MROffset.GetComponent<Camera>();
        Vector3 offsetPosition = MROffset.localPosition;
        Vector3 offsetPositionRec = offsetPosition;
        float fovRec = MRCamera.fieldOfView;

        if (Input.GetKey(KeyCode.W))
            offsetPosition.z += 0.01f;

        if (Input.GetKey(KeyCode.S))
            offsetPosition.z -= 0.01f;

        if (Input.GetKey(KeyCode.A))
            offsetPosition.x += 0.01f;

        if (Input.GetKey(KeyCode.D))
            offsetPosition.x -= 0.01f;

        if (Input.GetKey(KeyCode.Z))
            offsetPosition.y += 0.01f;

        if (Input.GetKey(KeyCode.X))
            offsetPosition.y -= 0.01f;

        if (Input.GetKey(KeyCode.C))
            MRCamera.fieldOfView += 0.1f;

        if (Input.GetKey(KeyCode.V))
            MRCamera.fieldOfView -= 0.1f;

        MROffset.localPosition = offsetPosition;

        if (offsetPositionRec != MROffset.localPosition || fovRec != MRCamera.fieldOfView)
        {
            Debug.LogWarning("MRCamera fov : " + MRCamera.fieldOfView);
            Debug.LogWarning("MRCamera offset : " + offsetPosition);
        }
    }
}
